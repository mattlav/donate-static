import React from 'react';
import {StripeProcessor} from './stripe_processor';

import {StripeProvider, Elements} from 'react-stripe-elements';

export function StripeWrapper(props) {
  const {stripePublishableKey, setErrors, formState, fieldsData, perkData, tokenData, frequency, donateProccessorBaseUrl, setLoading, successRedirectUrl, paymentMethod, onStripeFieldChange, errors, selectedPrice} = props;

  return (
    <StripeProvider apiKey={stripePublishableKey}>
      <Elements>
        <StripeProcessor formState={formState} setErrors={setErrors} fieldsData={fieldsData} perkData={perkData} tokenData={tokenData} frequency={frequency} donateProccessorBaseUrl={donateProccessorBaseUrl} setLoading={setLoading} successRedirectUrl={successRedirectUrl} paymentMethod={paymentMethod} errors={errors} onStripeFieldChange={onStripeFieldChange} selectedPrice={selectedPrice} />
      </Elements>
    </StripeProvider>
  );
}
