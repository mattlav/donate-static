import React from 'react';

export function Checkbox(props) {
  const {name, onChange, checked} = props;
  return (
    <input name={name} id={name} type="checkbox" onChange={onChange} checked={checked}/>
  );
}
