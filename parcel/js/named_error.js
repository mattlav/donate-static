export class NamedError {
  constructor(name, message = null) {
    this.name = name;
    this.message = message;
    this.id = name + message;
  }
}

export function findErrorByName(errors, name) {
  return errors.find((error) => error.name === name);
}
