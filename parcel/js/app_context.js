import React from 'react';

export const AppContext = React.createContext({});

export function initializeAppContext(props) {
  const {assetBaseUrl} = props;
  if (assetBaseUrl === undefined) {
    return {};
  }
  const parts = assetBaseUrl.split('?');
  const appContext = {
    assetBaseUrl: {
      path: parts[0],
      queryString: parts[1],
    }
  }
  return appContext;
}
