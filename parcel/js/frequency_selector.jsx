import React from 'react';
import {FrequencyOptionButton} from './frequency_option_button';

export function FrequencySelector(props) {
  const {frequencyOptions, frequency, onFrequencySelection} = props;

  const renderButtons = () => {
    if (frequencyOptions != 'both') {
      return null;
    }
    return (
      <div className="donate-options">
        <FrequencyOptionButton
          frequency={frequency}
          onFrequencySelection={onFrequencySelection}
          name="single"
        />
        <FrequencyOptionButton
          frequency={frequency}
          onFrequencySelection={onFrequencySelection}
          name="monthly"
        />
      </div>
    );
  };

  return renderButtons();
}
