var $ = require('jquery');

function GiftMatchingController() {
  this.setupInputPlaceHolderBehavior();
  this.watchForInputs();
}

GiftMatchingController.prototype.setupInputPlaceHolderBehavior = function() {
  var placeholder = 'Type Company Name';
  $('.donate-match-input').attr('placeholder', placeholder);
  $('.donate-match-input').on('focus', function() {
    $(this).attr('placeholder', '');
  }).on('focusout', function() {
    $('.donate-match-container .load-container').hide();
    $('.donate-error-message').remove();
    $('.donate-match-input').attr('placeholder', placeholder);
  });
}

GiftMatchingController.prototype.watchForInputs = function() {
  $('.donate-match-input').on('keyup', $.proxy(this.onInputKeyup, this));
}

GiftMatchingController.prototype.onInputKeyup = function() {

  $('.donate-match-container .load-container').show();
  this.displayErrorMessage("An error occurred. Please contact giving(at)torproject.org for matching gift details.");
}

GiftMatchingController.prototype.displayErrorMessage = function(message) {
  $('.donate-error-message').remove();
  $('<p class="donate-error-message">' + message + '</p>').prependTo('.donate-match-form');
}

module.exports = GiftMatchingController;
