import React from 'react';
import {WalletAddress} from './wallet_address';

export function WalletAddresses(props) {
  const {wallets} = props;

  const walletAddresses = wallets.map((wallet) => {
    return (<WalletAddress key={wallet.symbol} {...wallet} />);
  });
  return (
    <ul>
      {walletAddresses}
    </ul>
  );
}
